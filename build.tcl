variable mercury_module

if { $::argc > 0 } {
  for {set i 0} {$i < $::argc} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--mercury_module"   { incr i; set mercury_module [lindex $::argv $i] }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified\n"
          return 1
        }
      }
    }
  }
}

switch $mercury_module {
  XU5_4EV {
    open_project Enzian_BMC_XU5_4EV/Enzian_BMC_XU5_4EV.xpr
  }
  XU5_5EV {
    open_project Enzian_BMC_XU5_5EV/Enzian_BMC_XU5_5EV.xpr
  }
  default {
    puts "ERORR: Module $mercury_module not available."
    return 1
  }
}
launch_runs impl_1 -to_step write_bitstream -jobs 16
wait_on_run synth_1
wait_on_run impl_1
quit
