#*******************************************************************************
# This is a wrapper around the project creation script that can be exported
# from Vivado to let us select which Mercury module we're building for.
# The settings for the modules can be found in the reference design provided
# by Enclustra.
#
# The exported script needs to be modified as follows:
#   1. Delete the argument parsing part
#   2. Replace all occurences of the SoC part string with the ${part} variable
#   3. Insert the DDR_TYPE parameter switches from the Enclustra reference
#      design
# I've found the easiest way is to re-export the script after changes and then
# use the diff to find out which changes are relevant.
#*******************************************************************************

variable mercury_module

if { $::argc > 0 } {
  for {set i 0} {$i < $::argc} {incr i} {
    set option [string trim [lindex $::argv $i]]
    switch -regexp -- $option {
      "--mercury_module"   { incr i; set mercury_module [lindex $::argv $i] }
      default {
        if { [regexp {^-} $option] } {
          puts "ERROR: Unknown option '$option' specified\n"
          return 1
        }
      }
    }
  }
}

variable part
variable project_name
variable PS_DDR_TYPE
variable PL_DDR_TYPE

switch $mercury_module {
  XU5_4EV {
    set part xczu4ev-sfvc784-1-i
    set user_project_name "Enzian_BMC_XU5_4EV"
    set PS_DDR_TYPE PS_D11E
    set PL_DDR_TYPE 512MB_1066MHz
  }
  XU5_5EV {
    set part xczu5ev-sfvc784-2-i
    set user_project_name "Enzian_BMC_XU5_5EV"
    set PS_DDR_TYPE PS_D12E
    set PL_DDR_TYPE 1GB_1200MHz
  }
  default {
    puts "ERORR: Module $mercury_module not available."
    return 1
  }
}

# Source the modified Vivado exported script to create the project
source create_project.tcl
