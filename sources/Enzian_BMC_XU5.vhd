-- Copyright (c) 2022, Enclustra GmbH, Switzerland
-- Copyright (c) 2022, ETH Zurich
-- All rights reserved.
--
-- This file is distributed under the terms in the attached LICENSE file.
-- If you do not find this file, copies can be found by writing to:
-- ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
--
----------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- libraries
---------------------------------------------------------------------------------------------------
library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.vcomponents.all;

---------------------------------------------------------------------------------------------------
-- Naming convention in this file
---------------------------------------------------------------------------------------------------
-- Ports of the top-level are named in UPPER_CASE. See entity declaration below
-- Ports in the block diagram are in UPPER_CASE, plus the _lower_case_suffix appended by Vivado
-- Ports already in the Enclustra reference design are exceptions
-- STD_LOGIC, STD_LOGIC_VECTOR(A downto B)
-- No manual padding to align port types vertically
-- Indent in two spacess
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- entity declaration
---------------------------------------------------------------------------------------------------
-- This entity represents the whole design, whose port are 1-to-1 connected to the outside world
-- according to the XDC constraint. They connect either to componments on the Mercury XU5 module, 
-- or to the Enzian base board through the connectors. Interchangably, it can be viewed as the
-- outside world itself.
-- To ease the work of generating XDC file from specifications (for Enzian base board), ports
-- here are mostly named with the name as in the datasheets. See the XDC file generation scripts
-- for details.
---------------------------------------------------------------------------------------------------
entity Enzian_BMC_XU5 is
  generic (
    BG_WIDTH : natural
  );
  
  port (
    --------------------
    -- Mercury Module --
    --------------------    
    -- LED
    XU5_LED1_N : out STD_LOGIC;
    XU5_LED2_N : out STD_LOGIC;
    XU5_LED3_N : out STD_LOGIC;
    
    -- PL 100 MHz Oscillator
    CLK100_PL_N : in STD_LOGIC;
    CLK100_PL_P : in STD_LOGIC;
    
    -- PL DDR4 Memory
    DDR4PL_ACT_N : out STD_LOGIC;
    DDR4PL_RST_N : out STD_LOGIC;
    DDR4PL_BA : out STD_LOGIC_VECTOR(1 downto 0);
    DDR4PL_BG : out STD_LOGIC_VECTOR(1 downto 0);
    DDR4PL_CKE : out STD_LOGIC_VECTOR(0 downto 0);
    DDR4PL_DQ : inout STD_LOGIC_VECTOR(15 downto 0);
    DDR4PL_ODT : out STD_LOGIC_VECTOR(0 downto 0);
    DDR4PL_A : out STD_LOGIC_VECTOR(16 downto 0);
    DDR4PL_CK_N : out STD_LOGIC_VECTOR(0 downto 0);
    DDR4PL_CK_P : out STD_LOGIC_VECTOR(0 downto 0);
    DDR4PL_CS_N : out STD_LOGIC_VECTOR(0 downto 0);
    DDR4PL_DM : inout STD_LOGIC_VECTOR(1 downto 0);
    DDR4PL_DQS_N : inout STD_LOGIC_VECTOR(1 downto 0);
    DDR4PL_DQS_P : inout STD_LOGIC_VECTOR(1 downto 0);
    
    -- I2C bus routed to the PL slide, connected with PS, connector and EEPROM
    XU5_I2C_SDA : inout STD_LOGIC;
    XU5_I2C_SCL : inout STD_LOGIC;
    
    ------------
    -- Enzian --
    ------------
    -- Power/Fan I2C and pins
    B_PWR_FAN_I2C_SCL : inout STD_LOGIC;
    B_PWR_FAN_I2C_SDA : inout STD_LOGIC;
    B_PWR_ALERT_N : in STD_LOGIC;
    B_FAN_RESET_N : out STD_LOGIC;
    B_FAN_ALERT_N : in STD_LOGIC;
    B_FAN_FAULT_N : inout STD_LOGIC;

    -- Power sequencer (isppac) I2C bus and alert signal
    B_SEQ_I2C_SDA : inout STD_LOGIC;
    B_SEQ_I2C_SCL : inout STD_LOGIC;
    B_SEQ_ALERT_N : in STD_LOGIC;
    
    -- PSU I2C bus and alert signal
    B_PSU_I2C_SDA : inout STD_LOGIC;
    B_PSU_I2C_SCL : inout STD_LOGIC;
    B_PSU_ALERT_N : in STD_LOGIC;
    
    -- Clock generator I2C bus
    B_CLOCK_I2C_SDA : inout STD_LOGIC;
    B_CLOCK_I2C_SCL : inout STD_LOGIC;

    -- Front-panel IO
    B_FPIO_PWR_LED : out STD_LOGIC;
    B_FPIO_PWR_SW_N : in STD_LOGIC;
    B_FPIO_RST_SW_N : in STD_LOGIC;
    B_FPIO_SID_SW_N : in STD_LOGIC;
    B_FPIO_1WIRE : inout STD_LOGIC;
    B_FPIO_NMI_SW_N : in STD_LOGIC;
    B_FPIO_SID_LED : out STD_LOGIC;
    B_FPIO_F1_LED : out STD_LOGIC;
    B_FPIO_F2_LED : out STD_LOGIC;
    B_FPIO_NIC1_LED : out STD_LOGIC;
    B_FPIO_I2C_SDA : inout STD_LOGIC;
    B_FPIO_I2C_SCL : inout STD_LOGIC;
    B_FPIO_CI_SW_N : in STD_LOGIC;
    B_FPIO_NIC2_LED : out STD_LOGIC;

    -- Enzian USB UARTs
    B_UART0_CTS : in STD_LOGIC;
    B_UART0_RTS : out STD_LOGIC;
    
    B_UART1_RXD : in STD_LOGIC;
    B_UART1_TXD : out STD_LOGIC;
    B_UART1_CTS : in STD_LOGIC;
    B_UART1_RTS : out STD_LOGIC;
    
    B_UART2_RXD : in STD_LOGIC;
    B_UART2_TXD : out STD_LOGIC;
    B_UART2_CTS : in STD_LOGIC;
    B_UART2_RTS : out STD_LOGIC;
    
    B_UART3_RXD : in STD_LOGIC;
    B_UART3_TXD : out STD_LOGIC;
    B_UART3_CTS : in STD_LOGIC;
    B_UART3_RTS : out STD_LOGIC;

    -- UARTs to CPU (ThunderX)
    B_CUART0_RXD : in STD_LOGIC;
    B_CUART0_TXD : out STD_LOGIC;
    B_CUART0_CTS : in STD_LOGIC;
    B_CUART0_RTS : out STD_LOGIC;

    B_CUART1_RXD : in STD_LOGIC;
    B_CUART1_TXD : out STD_LOGIC;
    B_CUART1_CTS : in STD_LOGIC;
    B_CUART1_RTS : out STD_LOGIC;

    -- UART to FPGA (XCVU9P)
    B_FUART_RXD : in STD_LOGIC;
    B_FUART_TXD : out STD_LOGIC;
    B_FUART_CTS : in STD_LOGIC;
    B_FUART_RTS : out STD_LOGIC;
    
    -- CPU DRAM voltage control (parallel VID)
    B_CDV_VID : out STD_LOGIC_VECTOR(7 downto 0) ;

    -- FPGA (XCVU9P) DRAM voltage control (parallel VID)
    B_FDV_VID : out STD_LOGIC_VECTOR(7 downto 0);

    -- BMC user LEDs on baseboard
    B_USERIO_LED : out STD_LOGIC_VECTOR(8 downto 1);

    -- BMC user IO DIP switches on baseboard
    B_USERIO_SW_N : in STD_LOGIC_VECTOR(8 downto 1);
    
    -- CPU SPI boot flash
    B_SPI_SCK : out STD_LOGIC;
    B_SPI_CS_N : out STD_LOGIC;
    B_SPI_MOSI : out STD_LOGIC;
    B_SPI_MISO : in STD_LOGIC;
    B_SPI_SEL_N : out STD_LOGIC;

    -- FPGA (XCVU9P) serial configuration interface
    F_SSCONF_PROG_B : out STD_LOGIC;
    F_SSCONF_DATA : out STD_LOGIC;
    F_SSCONF_CLK : out STD_LOGIC;
    F_SSCONF_INIT_B : in STD_LOGIC;
    F_SSCONF_DONE : in STD_LOGIC;
    
    -- PSU control pin
    B_PSUP_ON : out STD_LOGIC;

    -- CPU pinstraps and reset control
    C_RESET_OUT_N : in STD_LOGIC;
    C_RESET_N : out STD_LOGIC;
    C_PLL_DCOK : out STD_LOGIC;
    B_OCI2_LNK1 : out STD_LOGIC;
    B_OCI3_LNK1 : out STD_LOGIC;
    
    -- Clock generator Loss-Of-Lock signals
    B_CLOCK_BLOL : in STD_LOGIC;
    B_CLOCK_CLOL : in STD_LOGIC;
    B_CLOCK_FLOL : in STD_LOGIC;
    
    -- FMC clock source selector
    B_FMCC_SEL : out STD_LOGIC;
    
    -- Interrupt pin from FPGA (XCVU9P)
    B_C2C_NMI : out STD_LOGIC;
    
    -- USB2 VBUS enable and overcurrent detect
    B_USB2_EN : out STD_LOGIC;
    B_USB2_OC_N : in STD_LOGIC;

    -- CPU NCSI (Ethernet sideband interface)
    B_NCSI_CRS_DV : in STD_LOGIC;
    B_NCSI_RX_ER : in STD_LOGIC;
    B_NCSI_RXD : in STD_LOGIC_VECTOR(1 downto 0);
    B_NCSI_TX_EN : out STD_LOGIC;
    B_NCSI_TXD : out STD_LOGIC_VECTOR(1 downto 0);
    B_NCSIC : in STD_LOGIC;
    
    -- CPU and FPGA DRAM ALERT signals
    F_D_EVENT_N : inout STD_LOGIC;
    C_D_EVENT_N : inout STD_LOGIC
  
    -- Chip-to-chip link between the BMC and the FPGA (not used yet)
    -- MGT_REFCLK0_P : in std_ulogic; 
    -- MGT_REFCLK0_N : in std_ulogic; 
    -- MGT_REFCLK1_P : in std_ulogic; 
    -- MGT_REFCLK1_N : in std_ulogic; 
            
    -- MGT_TX_P : out STD_LOGIC_VECTOR(3 downto 0);
    -- MGT_TX_N : out STD_LOGIC_VECTOR(3 downto 0); 
    -- MGT_RX_P : in STD_LOGIC_VECTOR(3 downto 0);
    -- MGT_RX_N : in STD_LOGIC_VECTOR(3 downto 0)
  );
end Enzian_BMC_XU5;

architecture rtl of Enzian_BMC_XU5 is

  ---------------------------------------------------------------------------------------------------
  -- component declarations
  ---------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------
  -- Mercury_XU5
  ---------------------------------------------------------------------------------------------------
  -- This component represents the block diagram. Pins are named as how Vivado generates ports from
  -- the interfaces/ports in the block diagram and must be matched exactly. One way to get these
  -- declarations is to let Vivado generates wrappers for the design (right click on the block 
  -- diagram - Create HDL Wrapper... - generate as a seperate file). Only used ports are preserved.
  ---------------------------------------------------------------------------------------------------
  component Mercury_XU5 is
    port (
      -- From PS
      Clk100 : out STD_LOGIC;
      Clk50 : out STD_LOGIC;
      Rst_N : out STD_LOGIC;

      -- GPIO_LED_N
      GPIO_LED_N : out STD_LOGIC_VECTOR(1 downto 0);

      -- C0_DDR4
      C0_SYS_CLK_clk_n : in STD_LOGIC;
      C0_SYS_CLK_clk_p : in STD_LOGIC;
      C0_DDR4_act_n : out STD_LOGIC;
      C0_DDR4_adr : out STD_LOGIC_VECTOR(16 downto 0);
      C0_DDR4_ba : out STD_LOGIC_VECTOR(1 downto 0);
      C0_DDR4_bg : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_ck_c : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_ck_t : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_cke : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_cs_n : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_dm_n : inout STD_LOGIC_VECTOR(1 downto 0);
      C0_DDR4_dq : inout STD_LOGIC_VECTOR(15 downto 0);
      C0_DDR4_dqs_c : inout STD_LOGIC_VECTOR(1 downto 0);
      C0_DDR4_dqs_t : inout STD_LOGIC_VECTOR(1 downto 0);
      C0_DDR4_odt : out STD_LOGIC_VECTOR(0 to 0);
      C0_DDR4_reset_n : out STD_LOGIC;

      -- GPIO_0
      GPIO_0_tri_i : in STD_LOGIC_VECTOR(31 downto 0);
      GPIO_0_tri_o : out STD_LOGIC_VECTOR(31 downto 0);
      GPIO_0_tri_t : out STD_LOGIC_VECTOR(31 downto 0);

      -- GPIO_1
      GPIO_1_tri_i : in STD_LOGIC_VECTOR(31 downto 0);
      GPIO_1_tri_o : out STD_LOGIC_VECTOR(31 downto 0);
      GPIO_1_tri_t : out STD_LOGIC_VECTOR(31 downto 0);

      -- GPIO_I2C
      GPIO_I2C_tri_i : in STD_LOGIC_VECTOR(9 downto 0);
      GPIO_I2C_tri_o : out STD_LOGIC_VECTOR(9 downto 0);
      GPIO_I2C_tri_t : out STD_LOGIC_VECTOR(9 downto 0);

      -- GPIO_CDV
      GPIO_CDV_tri_o : out STD_LOGIC_VECTOR(7 downto 0);

      -- GPIO_FDV
      GPIO_FDV_tri_o : out STD_LOGIC_VECTOR(7 downto 0);

      -- ENZIAN_UART_0
      ENZIAN_UART_0_ctsn : in STD_LOGIC;
      ENZIAN_UART_0_rtsn : out STD_LOGIC;

      -- ENZIAN_UART_1
      ENZIAN_UART_1_ctsn : in STD_LOGIC;
      ENZIAN_UART_1_rtsn : out STD_LOGIC;
      ENZIAN_UART_1_rxd : in STD_LOGIC;
      ENZIAN_UART_1_txd : out STD_LOGIC;

      -- ENZIAN_UART_2
      ENZIAN_UART_2_ctsn : in STD_LOGIC;
      ENZIAN_UART_2_rtsn : out STD_LOGIC;
      ENZIAN_UART_2_rxd : in STD_LOGIC;
      ENZIAN_UART_2_txd : out STD_LOGIC;

      -- ENZIAN_UART_3
      ENZIAN_UART_3_ctsn : in STD_LOGIC;
      ENZIAN_UART_3_rtsn : out STD_LOGIC;
      ENZIAN_UART_3_rxd : in STD_LOGIC;
      ENZIAN_UART_3_txd : out STD_LOGIC;

      -- CPU_UART_0
      CPU_UART_0_ctsn : in STD_LOGIC;
      CPU_UART_0_rtsn : out STD_LOGIC;
      CPU_UART_0_rxd : in STD_LOGIC;
      CPU_UART_0_txd : out STD_LOGIC;

      -- CPU_UART_1
      CPU_UART_1_ctsn : in STD_LOGIC;
      CPU_UART_1_rtsn : out STD_LOGIC;
      CPU_UART_1_rxd : in STD_LOGIC;
      CPU_UART_1_txd : out STD_LOGIC;

      -- FPGA_UART
      FPGA_UART_ctsn : in STD_LOGIC;
      FPGA_UART_rtsn : out STD_LOGIC;
      FPGA_UART_rxd : in STD_LOGIC;
      FPGA_UART_txd : out STD_LOGIC;

      -- FPGA_SSCONF
      FPGA_SSCONF_io0_o : out STD_LOGIC;
      FPGA_SSCONF_sck_o : out STD_LOGIC;

      -- ENZIAN_FLASH_SPI
      ENZIAN_FLASH_SPI_io0_o : out STD_LOGIC;
      ENZIAN_FLASH_SPI_io1_i : in STD_LOGIC;
      ENZIAN_FLASH_SPI_sck_o : out STD_LOGIC;
      ENZIAN_FLASH_SPI_ss_o : out STD_LOGIC_VECTOR(0 to 0)

      -- NCSI_MII
      -- NCSI_MII_col : in STD_LOGIC;
      -- NCSI_MII_crs : in STD_LOGIC;
      -- NCSI_MII_rst_n : out STD_LOGIC;
      -- NCSI_MII_rx_clk : in STD_LOGIC;
      -- NCSI_MII_rx_dv : in STD_LOGIC;
      -- NCSI_MII_rx_er : in STD_LOGIC;
      -- NCSI_MII_rxd : in STD_LOGIC_VECTOR(3 downto 0);
      -- NCSI_MII_tx_clk : in STD_LOGIC;
      -- NCSI_MII_tx_en : out STD_LOGIC;
      -- NCSI_MII_txd : out STD_LOGIC_VECTOR(3 downto 0);      

      -- Something not yet working
      -- MGT_REFCLK0_P : in std_ulogic; 
      -- MGT_REFCLK0_N : in std_ulogic; 
      -- MGT_REFCLK1_P : in std_ulogic; 
      -- MGT_REFCLK1_N : in std_ulogic; 
      -- MGT_TX_P : out STD_LOGIC_VECTOR(3 downto 0);
      -- MGT_TX_N : out STD_LOGIC_VECTOR(3 downto 0); 
      -- MGT_RX_P : in STD_LOGIC_VECTOR(3 downto 0);
      -- MGT_RX_N : in STD_LOGIC_VECTOR(3 downto 0)
    );
  end component Mercury_XU5;

  ---------------------------------------------------------------------------------------------------
  -- signal declarations
  ---------------------------------------------------------------------------------------------------
  -- Here we declare signals as few as possible. For example, if an output of Mercury_XU5 is directly
  -- routed to a port of the top-level, we do not declare an signal as the intermediate. Most of 
  -- these signals are wires for tri states between Mercury_XU5 and IOBUFs.
  -- If signals are used in process ... end process, they are initiated as registers. Otherwise 
  -- simply wires.
  ---------------------------------------------------------------------------------------------------
  signal Clk100 : STD_LOGIC;
  signal Clk50 : STD_LOGIC;
  signal Rst_N : STD_LOGIC;

  signal GPIO_LED_N : STD_LOGIC_VECTOR(1 downto 0);
  signal LedCount : unsigned(23 downto 0);  -- used as registers

  signal GPIO_0_tri_i : STD_LOGIC_VECTOR(31 downto 0);
  signal GPIO_0_tri_o : STD_LOGIC_VECTOR(31 downto 0);
  signal GPIO_0_tri_t : STD_LOGIC_VECTOR(31 downto 0);
  signal GPIO_1_tri_i : STD_LOGIC_VECTOR(31 downto 0);
  signal GPIO_1_tri_o : STD_LOGIC_VECTOR(31 downto 0);
  signal GPIO_1_tri_t : STD_LOGIC_VECTOR(31 downto 0);
  
  signal GPIO_I2C_tri_i : STD_LOGIC_VECTOR(9 downto 0);
  signal GPIO_I2C_tri_o : STD_LOGIC_VECTOR(9 downto 0);
  signal GPIO_I2C_tri_t : STD_LOGIC_VECTOR(9 downto 0);
  
  signal FAN_FAULT_N_tri_i : STD_LOGIC;
  signal FAN_FAULT_N_tri_o : STD_LOGIC;
  signal FAN_FAULT_N_tri_t : STD_LOGIC;

  signal C_D_EVENT_N_tri_i : STD_LOGIC;
  signal C_D_EVENT_N_tri_o : STD_LOGIC;
  signal C_D_EVENT_N_tri_t : STD_LOGIC;

  signal F_D_EVENT_N_tri_i : STD_LOGIC;
  signal F_D_EVENT_N_tri_o : STD_LOGIC;
  signal F_D_EVENT_N_tri_t : STD_LOGIC;

  signal F_FPIO_1WIRE_tri_i: STD_LOGIC;
  signal F_FPIO_1WIRE_tri_o: STD_LOGIC;
  signal F_FPIO_1WIRE_tri_t: STD_LOGIC;

begin
  
  ---------------------------------------------------------------------------------------------------
  -- processor system instance
  ---------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------
  -- How routings are specified in the following code
  ---------------------------------------------------------------------------------------------------
  -- The routings are achieved by a combination of the port mapping of Mercury_XU5_i below, IOBUFs
  -- connections, and the manual routings that follows. For example, a port of Mercury_XU5_i can 
  -- directly route to a port of the top-level. Or, an IOBUF's IO pin is routed to a port. Or, a
  -- top-level port is connected to the input pin of an GPIO, etc.
  ---------------------------------------------------------------------------------------------------

  Mercury_XU5_i: component Mercury_XU5
    port map (
  --  Component port => signal or entity Enzian_BMC_XU5 port

      Clk100 => Clk100,
      Clk50 => Clk50,
      Rst_N => Rst_N,

      GPIO_LED_N => GPIO_LED_N,

      C0_SYS_CLK_clk_n => CLK100_PL_N,
      C0_SYS_CLK_clk_p => CLK100_PL_P,
      C0_DDR4_act_n => DDR4PL_ACT_N,
      C0_DDR4_reset_n => DDR4PL_RST_N,
      C0_DDR4_ba => DDR4PL_BA,
      C0_DDR4_bg => DDR4PL_BG ( BG_WIDTH-1 downto 0 ),
      C0_DDR4_cke => DDR4PL_CKE,
      C0_DDR4_dq => DDR4PL_DQ,
      C0_DDR4_odt => DDR4PL_ODT,
      C0_DDR4_adr => DDR4PL_A,
      C0_DDR4_ck_c => DDR4PL_CK_N,
      C0_DDR4_ck_t => DDR4PL_CK_P,
      C0_DDR4_cs_n => DDR4PL_CS_N,
      C0_DDR4_dm_n => DDR4PL_DM,
      C0_DDR4_dqs_c => DDR4PL_DQS_N,
      C0_DDR4_dqs_t => DDR4PL_DQS_P,

      GPIO_0_tri_i => GPIO_0_tri_i, 
      GPIO_0_tri_o => GPIO_0_tri_o, 
      GPIO_0_tri_t => GPIO_0_tri_t, 
      GPIO_1_tri_i => GPIO_1_tri_i, 
      GPIO_1_tri_o => GPIO_1_tri_o, 
      GPIO_1_tri_t => GPIO_1_tri_t, 
      
      GPIO_I2C_tri_i => GPIO_I2C_tri_i,
      GPIO_I2C_tri_o => GPIO_I2C_tri_o,
      GPIO_I2C_tri_t => GPIO_I2C_tri_t,

      GPIO_CDV_tri_o => B_CDV_VID,
      GPIO_FDV_tri_o => B_FDV_VID,

      -- Some B_UARTs (ENZIAN_UARTs) are actually routed to CPU_UARTs/FPGA_UART below
      -- The UART controllers and these routing rules are preserved in case these
      -- ports needs to be monitored/driven by the BMC via software.

      -- USB UART0, only modem signals go to FPGA
      ENZIAN_UART_0_ctsn => B_UART0_CTS,
      ENZIAN_UART_0_rtsn => B_UART0_RTS,

      -- USB UART1 routed directly below
      ENZIAN_UART_1_ctsn => '1',
      -- ENZIAN_UART_1_rtsn => ,
      ENZIAN_UART_1_rxd => '0',
      -- ENZIAN_UART_1_txd => ,

      -- USB UART1 routed in block diagram
      ENZIAN_UART_2_ctsn => B_UART2_CTS,
      ENZIAN_UART_2_rtsn => B_UART2_RTS,
      ENZIAN_UART_2_rxd => B_UART2_RXD,
      ENZIAN_UART_2_txd => B_UART2_TXD,

      -- USB UART2 routed directly below
      ENZIAN_UART_3_ctsn => '1',
      -- ENZIAN_UART_3_rtsn => ,
      ENZIAN_UART_3_rxd => '0',
      -- ENZIAN_UART_2_txd => ,

      -- CPU UART0 routed directly below
      CPU_UART_0_ctsn => '1',
      -- CPU_UART_0_rtsn => ,
      CPU_UART_0_rxd => '0',
      -- CPU_UART_0_txd => ,

      -- CPU UART1 routed in block diagram
      CPU_UART_1_ctsn => B_CUART1_CTS,
      CPU_UART_1_rtsn => B_CUART1_RTS,
      CPU_UART_1_rxd => B_CUART1_RXD,
      CPU_UART_1_txd => B_CUART1_TXD,

      -- FPGA UART routed directly below
      FPGA_UART_ctsn => '1',
      -- FPGA_UART_rtsn => ,
      FPGA_UART_rxd => '0',
      -- FPGA_UART_txd => ,

      FPGA_SSCONF_io0_o => F_SSCONF_DATA,
      FPGA_SSCONF_sck_o => F_SSCONF_CLK,

      ENZIAN_FLASH_SPI_io0_o => B_SPI_MOSI,
      ENZIAN_FLASH_SPI_io1_i => B_SPI_MISO,
      ENZIAN_FLASH_SPI_sck_o => B_SPI_SCK,
      ENZIAN_FLASH_SPI_ss_o(0) => B_SPI_CS_N
    );
  
  -- Blink LED1 driven by Clk50
  process (Clk50)
  begin
    if rising_edge (Clk50) then
      if Rst_N = '0' then
        LedCount <= (others => '0');
      else
        LedCount <= LedCount + 1;
      end if;
    end if;
  end process;
  XU5_LED1_N <= '0' when LedCount(LedCount'high) = '0' else 'Z';
  XU5_LED2_N <= '0' when GPIO_LED_N(0) = '0' else 'Z';
  XU5_LED3_N <= '0' when GPIO_LED_N(1) = '0' else 'Z';
  
  -- IOBUFs
  -- NOTICE: I to tri_o and O to tri_i
  SEQ_I2C_SDA_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(0),
      O => GPIO_I2C_tri_i(0),
      T => GPIO_I2C_tri_t(0),
      IO => B_SEQ_I2C_SDA
    );
  SEQ_I2C_SCL_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(1),
      O => GPIO_I2C_tri_i(1),
      T => GPIO_I2C_tri_t(1),
      IO => B_SEQ_I2C_SCL
    );
  PSU_I2C_SDA_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(2),
      O => GPIO_I2C_tri_i(2),
      T => GPIO_I2C_tri_t(2),
      IO => B_PSU_I2C_SDA
    );
  PSU_I2C_SCL_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(3),
      O => GPIO_I2C_tri_i(3),
      T => GPIO_I2C_tri_t(3),
      IO => B_PSU_I2C_SCL
    );
  CLOCK_I2C_SDA_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(4),
      O => GPIO_I2C_tri_i(4),
      T => GPIO_I2C_tri_t(4),
      IO => B_CLOCK_I2C_SDA
    );
  CLOCK_I2C_SCL_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(5),
      O => GPIO_I2C_tri_i(5),
      T => GPIO_I2C_tri_t(5),
      IO => B_CLOCK_I2C_SCL
    );
  FPIO_I2C_SDA_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(6),
      O => GPIO_I2C_tri_i(6),
      T => GPIO_I2C_tri_t(6),
      IO => B_FPIO_I2C_SDA
    );
  FPIO_I2C_SCL_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(7),
      O => GPIO_I2C_tri_i(7),
      T => GPIO_I2C_tri_t(7),
      IO => B_FPIO_I2C_SCL
    );
  PWR_FAN_I2C_SDA_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(8),
      O => GPIO_I2C_tri_i(8),
      T => GPIO_I2C_tri_t(8),
      IO => B_PWR_FAN_I2C_SDA
    );
  PWR_FAN_I2C_SCL_iobuf : IOBUF
    port map (
      I => GPIO_I2C_tri_o(9),
      O => GPIO_I2C_tri_i(9),
      T => GPIO_I2C_tri_t(9),
      IO => B_PWR_FAN_I2C_SCL
    );
  FAN_FAULT_iobuf : IOBUF
    port map (
      I => FAN_FAULT_N_tri_o,
      O => FAN_FAULT_N_tri_i,
      T => FAN_FAULT_N_tri_t,
      IO => B_FAN_FAULT_N
     );
  C_D_EVENT_N_iobuf : IOBUF
    port map (
      I => C_D_EVENT_N_tri_o,
      O => C_D_EVENT_N_tri_i,
      T => C_D_EVENT_N_tri_t,
      IO => C_D_EVENT_N
   );
  F_D_EVENT_N_iobuf : IOBUF
   port map (
     I => F_D_EVENT_N_tri_o,
     O => F_D_EVENT_N_tri_i,
     T => F_D_EVENT_N_tri_t,
     IO => F_D_EVENT_N
  );
  FPIO_1WIRE_iobuf : IOBUF
    port map (
      I => F_FPIO_1WIRE_tri_o,
      O => F_FPIO_1WIRE_tri_i,
      T => F_FPIO_1WIRE_tri_t,
      IO => B_FPIO_1WIRE
   );

  -------------------
  -- UARTs routing --
  -------------------

  -- USB UART1 directly connected to CPU UART0
  B_UART1_TXD <= B_CUART0_RXD;
  B_CUART0_TXD <= B_UART1_RXD;
  B_UART1_RTS <= B_CUART0_CTS;
  B_CUART0_RTS <= B_UART1_CTS;
  
  -- USB UART3 directly connected to FPGA UART
  B_UART3_TXD <= B_FUART_RXD;
  B_FUART_TXD <= B_UART3_RXD;
  B_UART3_RTS <= B_FUART_CTS;
  B_FUART_RTS <= B_UART3_CTS;

  ----------
  -- GPIO --
  ----------

  -- GPIO port 0 inputs
  GPIO_0_tri_i(0) <= F_SSCONF_INIT_B;
  GPIO_0_tri_i(1) <= F_SSCONF_DONE;
  GPIO_0_tri_i(3) <= C_RESET_OUT_N;
  GPIO_0_tri_i(4) <= B_CLOCK_BLOL;
  GPIO_0_tri_i(5) <= B_CLOCK_CLOL;
  GPIO_0_tri_i(6) <= B_CLOCK_FLOL;
  GPIO_0_tri_i(8) <= B_USB2_OC_N;
  GPIO_0_tri_i(23 downto 16) <= B_USERIO_SW_N(8 downto 1);
  GPIO_0_tri_i(24) <= B_PWR_ALERT_N;
  GPIO_0_tri_i(25) <= B_SEQ_ALERT_N;
  GPIO_0_tri_i(26) <= B_PSU_ALERT_N;
  GPIO_0_tri_i(27) <= B_FAN_ALERT_N;

  -- GPIO port 0 outputs
  F_SSCONF_PROG_B <= GPIO_0_tri_o(2);
  B_PSUP_ON <= GPIO_0_tri_o(9);

  -- GPIO port 0 tri
  C_D_EVENT_N_tri_i <= GPIO_0_tri_i(10);
  C_D_EVENT_N_tri_o <= GPIO_0_tri_o(10);
  C_D_EVENT_N_tri_t <= GPIO_0_tri_t(10);
  F_D_EVENT_N_tri_i <= GPIO_0_tri_i(11);
  F_D_EVENT_N_tri_o <= GPIO_0_tri_o(11);
  F_D_EVENT_N_tri_t <= GPIO_0_tri_t(11);
  FAN_FAULT_N_tri_i <= GPIO_0_tri_i(28);
  FAN_FAULT_N_tri_o <= GPIO_0_tri_o(28);
  FAN_FAULT_N_tri_t <= GPIO_0_tri_t(28);
  

  -- GPIO port 1 inputs
  GPIO_1_tri_i(3) <= B_FPIO_PWR_SW_N;
  GPIO_1_tri_i(4) <= B_FPIO_RST_SW_N;
  GPIO_1_tri_i(5) <= B_FPIO_SID_SW_N;
  GPIO_1_tri_i(7) <= B_FPIO_NMI_SW_N;
  GPIO_1_tri_i(12) <= B_FPIO_CI_SW_N;

  -- GPIO port 1 outputs
  B_SPI_SEL_N <= GPIO_1_tri_o(0);
  B_FPIO_PWR_LED <= GPIO_1_tri_o(2);
  B_FPIO_SID_LED <= GPIO_1_tri_o(8);
  B_FPIO_F1_LED <= GPIO_1_tri_o(9);
  B_FPIO_F2_LED <= GPIO_1_tri_o(10);
  B_FPIO_NIC1_LED <= GPIO_1_tri_o(11);
  B_FPIO_NIC2_LED <= GPIO_1_tri_o(13);
  B_USERIO_LED(8 downto 1) <= GPIO_1_tri_o(23 downto 16);
  C_RESET_N <= GPIO_1_tri_o(24);
  C_PLL_DCOK <= GPIO_1_tri_o(25);
  B_OCI2_LNK1 <= GPIO_1_tri_o(26);
  B_OCI3_LNK1 <= GPIO_1_tri_o(27);
  B_C2C_NMI <= GPIO_1_tri_o(28);
  B_FMCC_SEL <= GPIO_1_tri_o(29);
  B_FAN_RESET_N <= GPIO_1_tri_o(30);
  B_USB2_EN <= GPIO_1_tri_o(31);

  -- GPIO port 1 tri
  F_FPIO_1WIRE_tri_i <= GPIO_1_tri_i(6);
  F_FPIO_1WIRE_tri_o <= GPIO_1_tri_o(6);
  F_FPIO_1WIRE_tri_t <= GPIO_1_tri_t(6);
end rtl;
